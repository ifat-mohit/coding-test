This problems are tested using python version 3.7.4

##### Command to run the test cases
```bash
$ python3 test.py
```
##### If it pass all the test cases
```bash
.........
----------------------------------------------------------------------
Ran 9 tests in 0.001s

OK

```

##### Explanation of Runtime and space complexity of 3rd problem 
***Time-Complexity:*** This method will require one traversal of the binary tree. So the Time-Complexity will be `O(m+n) ≈ O(n)` where m is the number of edges and n is the total number of nodes.

***Space-Complexity:*** Since, this implementation does not use any data structure to store the ancestors of the nodes. Hence, the space-complexity will be `O(n)`.
