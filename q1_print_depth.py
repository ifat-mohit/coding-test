def print_depth(dic: dict, dep: int = 1) -> None:
    ''' Print all keys of dictionary alongside with their depth '''

    for key in dic.keys():
        if isinstance(dic[key], dict):
            print(key, dep)
            print_depth(dic[key], dep+1)
        else:
            print(key, dep)
