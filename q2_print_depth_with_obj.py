from typing import Type

class Person:
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father


def print_depth(dic: dict, dep: int = 1, obj: Type[Person] = None) -> None:
    for key in dic.keys():
        if isinstance(dic[key], dict):
            print(f'{key} {dep}')
            print_depth(dic[key], dep+1)
        elif isinstance(dic[key], Person):
            print(f'{key}: {dep}')
            print_depth(vars(dic[key]), dep+1, dic[key])
        else:
            if hasattr(obj, key):
                print(f'{key}: {dep}')
            else:
                print(f'{key} {dep}')

