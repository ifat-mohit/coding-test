class Node:
	def __init__(self, val):
		self.val = val
		self.left = None
		self.right = None
		self.parent = None


class BinaryTree:
	def __init__(self,val):
		self.root = Node(val)


def least_common_ancestor(curr, node1, node2):
	if (curr == None):
		return None
	elif(curr == node1 or curr == node2):
		return curr

	left_subtree = least_common_ancestor(curr.left, node1, node2)
	right_subtree = least_common_ancestor(curr.right, node1, node2)

	if(left_subtree != None and right_subtree != None):
		return curr;
	elif(left_subtree != None):
		return left_subtree
	else :
		return right_subtree
