import io
import unittest
from unittest import mock
from q2_print_depth_with_obj import Person
from q3_least_common_ancestor import Node, BinaryTree
from q1_print_depth import print_depth
from q2_print_depth_with_obj import print_depth as print_depth_with_obj
from q3_least_common_ancestor import least_common_ancestor



class TestPrintDepth(unittest.TestCase):
    def setUp(self):
        self.input_1 = {'key1': 1,'key2': {'key3': 1,'key4': {'key5': 4}}}
        self.input_2 = {'key1': 1,'key2': {'key3': 1,'key4': 5,'key5': {'key5': 4}},'key6': 6}
        self.input_3 = {}
        self.out1 = 'key1 1\nkey2 1\nkey3 2\nkey4 2\nkey5 3\n'
        self.out2 = 'key1 1\nkey2 1\nkey3 2\nkey4 2\nkey5 2\nkey5 3\nkey6 1\n'
        self.out3 = ''

    @staticmethod
    def mock_print_func(func, inp):
        with mock.patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            func(inp)
            out = fake_stdout.getvalue()
        return out

    def test_print_depth_for_input_1(self):
        fake_out = TestPrintDepth.mock_print_func(print_depth, self.input_1)
        self.assertEqual(fake_out, self.out1)
        out_wrong_val = self.out1[5:]
        self.assertNotEqual(fake_out, out_wrong_val)

    def test_print_depth_for_input_2(self):
        fake_out = TestPrintDepth.mock_print_func(print_depth, self.input_2)
        self.assertEqual(fake_out, self.out2)
        out_wrong_val = self.out2[5:]
        self.assertNotEqual(fake_out, out_wrong_val)

    def test_print_depth_for_input_3(self):
        fake_out = TestPrintDepth.mock_print_func(print_depth, self.input_3)
        self.assertEqual(fake_out, self.out3)
        self.assertNotEqual(fake_out, self.out1)



class TestPrintDepthWithObject(unittest.TestCase):
    def setUp(self):
        self.person_a = Person('User', '1', None)
        self.person_b = Person('User', '2', self.person_a)
        self.input_1 = {'key1': 1, 'key2': {'key3': 1,'key4': {'key5': 4,'user': self.person_b}}}
        self.input_2 = {'key1': 1,'user': self.person_a,'key2': {'key3': 1,'key4': {'key5': 4,'user': self.person_b}}}
        self.out1 = 'key1 1\nkey2 1\nkey3 2\nkey4 2\nkey5 3\nuser: 3\nfirst_name: 4\nlast_name: 4\nfather: 4\nfirst_name: 5\nlast_name: 5\nfather: 5\n'
        self.out2 = 'key1 1\nuser: 1\nfirst_name: 2\nlast_name: 2\nfather: 2\nkey2 1\nkey3 2\nkey4 2\nkey5 3\nuser: 3\nfirst_name: 4\nlast_name: 4\nfather: 4\nfirst_name: 5\nlast_name: 5\nfather: 5\n'
        
        # since funciton-2 is the extended version of function-1 then it should support all the funcitonality of function-1
        self.input_3 = {'key1': 1,'key2': {'key3': 1,'key4': {'key5': 4}}}
        self.out3 = 'key1 1\nkey2 1\nkey3 2\nkey4 2\nkey5 3\n'


    def test_print_depth_for_input_1(self):
        fake_out1 = TestPrintDepth.mock_print_func(print_depth_with_obj, self.input_1)
        self.assertEqual(fake_out1, self.out1)
        out1_wrong_val = self.out1[10:]
        self.assertNotEqual(fake_out1, out1_wrong_val)


    def test_print_depth_for_input_2(self):
        fake_out = TestPrintDepth.mock_print_func(print_depth_with_obj, self.input_2)
        self.assertEqual(fake_out, self.out2)
        out_wrong_val = self.out2[10:]
        self.assertNotEqual(fake_out, out_wrong_val)
    
    def test_print_depth_for_input_3(self):
        fake_out = TestPrintDepth.mock_print_func(print_depth_with_obj, self.input_3)
        self.assertEqual(fake_out, self.out3)
        out_wrong_val = self.out3[10:]
        self.assertNotEqual(fake_out, out_wrong_val)



class TestLeastCommonAncestor(unittest.TestCase):
    def setUp(self):
        ''' Constructing the tree
                         1
                      /     \
                    2         3
                  /   \     /   \
                 4      5  6     7
               /   \   
              8     9

         '''
        self.BT = BinaryTree(1)
        self.BT.root.left = Node(2)
        self.BT.root.left.parent = self.BT.root
        self.BT.root.right = Node(3)
        self.BT.root.right.parent = self.BT.root
        self.BT.root.left.left = Node(4)
        self.BT.root.left.left.parent = self.BT.root.left
        self.BT.root.left.right = Node(5)
        self.BT.root.left.right.parent = self.BT.root.left
        self.BT.root.right.left = Node(6)
        self.BT.root.right.left.parent = self.BT.root.right
        self.BT.root.right.right = Node(7)
        self.BT.root.right.right.parent = self.BT.root.right
        self.BT.root.left.left.left = Node(8)
        self.BT.root.left.left.left.parent = self.BT.root.left
        self.BT.root.left.left.right = Node(9)
        self.BT.root.left.left.right.parent = self.BT.root.left

    def test_least_common_ancestor_with_different_node(self):
        result = least_common_ancestor(self.BT.root, self.BT.root.right.left, self.BT.root.right.right)
        self.assertEqual(result.val, 3)
        self.assertNotEqual(result.val, 1)
        
        result2 = least_common_ancestor(self.BT.root, self.BT.root.right.right, self.BT.root.left.left.right)
        self.assertEqual(result2.val, 1)
        self.assertNotEqual(result2.val, 3)

    def test_least_common_ancestor_with_same_node(self):
        result = least_common_ancestor(self.BT.root, self.BT.root.left.left.left, self.BT.root.left.left.left)
        self.assertEqual(result.val, 8)
        self.assertNotEqual(result.val, 4)

        result2 = least_common_ancestor(self.BT.root, self.BT.root.left.right, self.BT.root.left.right)
        self.assertEqual(result2.val, 5)
        self.assertNotEqual(result2.val, 4)

    def test_least_common_ancestor_with_none(self):
        result = least_common_ancestor(None, self.BT.root.left.left.left, self.BT.root.left.left.left)
        self.assertEqual(result, None)
        self.assertNotEqual(result, 4)

if __name__ == '__main__':
    unittest.main()